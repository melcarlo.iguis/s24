db.inventory.insertMany([
	{
		"name" : "Captain America Shield",
		"price" : 50000,
		"qty" : 17,
		"company" : "Hydra and Co"
	},
	{
		"name" : "Mjolnir",
		"price" : 75000,
		"qty" : 24,
		"company" : "Asgard Production"
	},
	{
		"name" : "Iron Man Suit",
		"price" : 25400,
		"qty" : 25,
		"company" : "Stark Industries"
	},
	{
		"name" : "Eye of Agamotto",
		"price" : 28000,
		"qty" : 51,
		"company" : "Sanctum Company"
	},
	{
		"name" : "Iron Spider Suit",
		"price" : 30000,
		"qty" : 24,
		"company" : "Stark Industries"
	}
])

// Query Operators 
	// allows us to be more flexible when querying in MongoDB , we can opt to find , update and delete documents base on some condition instead of just specific criteria.

/*
	Comparison Query Operators
		// $gt and $gte
		 db.collections.find({field: {$gt: value}})
		 db.collections.find({field: {$gte: value}})

		 $gt . greater than - allows us to find the values that are greater than the given value
		 $gt . greater than - allows us to find the values that are greater than or equal to the given value
*/

	db.inventory.find({"price" : {$gte : 75000}});

/*
	$lt and $lte
	$lt - it's allows us to find value that are less than than the given value
	$lte - it's allows us to find value that are less than or equal to the given value
	db.collections.find({field : {$lte : value}});
*/

	db.inventory.find({"qty" : {$lt : 20}});


/*

	$ne
	Syntax

	$ne - it's allows us to find value that are not equal to the given value
	db.collections.find({field : {$ne : value}});

*/

	db.inventory.find({"qty" : {$ne : 10}});

/*

	$in
	syntax
	db.collections.find({field : {$in : value}})

		$in - allows us to find documents that satisfy either of the specified value
*/

	db.inventory.find({"price" : {$in : [25400,30000]}});


	// Reminder  - although , you can express this query using $or operator, 

// Mini Activity


	// 
	db.inventory.find({"price" : {$gte : 50000}});


	// 2
	db.inventory.find({"qty" : {$in :[24, 16]}});


	// UPDATE AND DELETE
		// 

	db.inventory.updateMany({"qty" : {$gte : 24}}, {$set : {isActive : true}});

	// 3
	db.inventory.updateMany({"price" : {$lt : 28000}} , {$set : {"qty" : 17}});


// Logical Operators
	/*
		$and
		syntax

		db.collections.find({$and : [{criteria1} , {criteria2}]});

		$and - allows us to return documents that satisfies all given conditions.
	*/

	db.inventory.find({$and : 
		[
			{"price" : {$gte : 50000}},
			{"qty" : 17}
		]
    })

/*
	$or
	syntax
	db.collection.find({$or :
	[
		{criteria1},
		{criteria2}
	]
	})

	$or - allows us to return documents that satisfies one given conditions.
*/

	db.inventory.find({$or :
		[
			{"qty" : {$lt : 24}},
			{"isActive" : false}
		]	
	});


	// Mini activity
	db.inventory.updateMany({$or :
		[
			{"qty" : {$lte : 24}},
			{"price" : {$lte : 30000}}
		]},
		{$set : {"isActive" : true}}
	);

/*
	Evaluation Query Operator
		$regex
		syntax - {field : {$regex : /pattern/ }}
					case sensetive query 
				 {field :{$regex : /pattern/, $options: $optionValue}}
					case insensetive query
*/

	db.inventory.find(
		{
			"name" : {$regex : 'S'}
		}
	);

	db.inventory.find(
		{
			"name" : {$regex : 'A'}
		}
	);

	db.inventory.find(
		{
			"name" : {$regex : 'A' , $options : '$i'}
		}
	);

	db.inventory.find(
		{
			"name" : {$regex : /['A-C']/}
		}
	);


// Mini Activity
	
	db.inventory.find({$and :
		[
			{"name" : {$regex : 'i', $options : '$i'}},
			{"price" : {$gt : 70000}}
		]
	})


	db.inventory.find({$and :
		[
			{"name" : {$regex : 'i'}},
			{"price" : {$gt : 70000}}
		]
	})


/*
	Field Projection

		allows us to hide or show properties of a return documents after a query. when dealing with a complex data structures, there might be instances that fields are might be usefull for the query that we are trying to accomplish.

		Inclusion and Exclusion
			syntax
			db.collections.find({criteria}, {field : 1})
				field 1 = include and  0 = exclude
*/



	db.inventory.find({}, {"name" : 1, "_id" : 0});

	db.inventory.find({}, {"qty" : 0, "_id" : 0});

	db.inventory.find({}, {"_id" : 0 , "name" : 0, "price": 0});

	db.inventory.find({}, {"_id" : 0 , "name" : 1, "price": 1});



	// db.inventory.find(
	// 	{
	// 		"company" : {$regex : 'A'
	// 	},
	// 		"name" : 1, "company" : 1, "_id" : 0
	// 	}
	// )

	db.inventory.find({"company" : {$regex :'A'}}, {"name" : 1, "company" :1 , "_id" :0})

	db.inventory.find(
		{
			$and: 
			[
				{
					"name" : {$regex : 'A'},
				},
				{
					"price" : {$lte : 30000}
				}
			]
		},

			{
				"name" : 1, "price" : 1, "_id":0
			}
		)